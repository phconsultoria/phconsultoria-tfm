output "user_name" {
  value = data.aws_identitystore_user.this.user_name
}
output "user_id" {
  value = data.aws_identitystore_user.this.user_id
}
output "id" {
  value = data.aws_identitystore_user.this.id
}