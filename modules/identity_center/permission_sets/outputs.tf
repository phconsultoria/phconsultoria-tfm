output "sso_instance_arn" {
  value = local.sso_instance_arn
}
output "identity_store_id" {
  value = local.identity_store_id
}