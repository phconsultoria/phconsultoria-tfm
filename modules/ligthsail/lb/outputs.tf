output "lb_id" {
  value = aws_lightsail_lb.this.id
}
output "lb_name" {
  value = aws_lightsail_lb.this.name
}