output "cloudtrail_arn" {
  value = aws_cloudtrail.this.arn
}
output "cloudtrail_name" {
  value = aws_cloudtrail.this.name
}
output "cloudtrail_id" {
  value = aws_cloudtrail.this.id
}
