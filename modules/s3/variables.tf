# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED VARIABLES
# These variables must be set when using this module.
# ---------------------------------------------------------------------------------------------------------------------

variable "default_tags" {
  type        = map(string)
  description = "Default Tags"
  default = {
    CreatedBy       = "Terraform"
    TerraformModule = "https://gitlab.com/phconsultoria/phconsultoria-tfm/-/tree/main/modules/s3"
  }
}
variable "bucket_name" {
  type        = string
  description = "Bucket name"
}
variable "bucket_description" {
  type        = string
  description = "Description for Bucket S3"
}
variable "kms_master_key_id" {
  type        = string
  description = "KMS Key ID"
  default     = null
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL VARIABLES
# These variables have defaults, but may be overridden.
# ---------------------------------------------------------------------------------------------------------------------

variable "policy" {
  type        = string
  description = "Your policy or your file policy"
  default     = null
}
variable "force_destroy" {
  type        = bool
  description = "(Optional, Default:false) Boolean that indicates all objects (including any locked objects) should be deleted from the bucket when the bucket is destroyed so that the bucket can be destroyed without error"
  default     = false
  validation {
    condition     = var.force_destroy == true || var.force_destroy == false
    error_message = "Value should be true or false"
  }
}
variable "versioning_status" {
  type        = string
  description = "Set Enabled or Disabled for object versioning"
  default     = "Disabled"
  validation {
    condition     = var.versioning_status == "Disabled" || var.versioning_status == "Enabled" || var.versioning_status == "Suspended"
    error_message = "Value should be Disbled, Enabled or Suspended"
  }
}
variable "cors_configuration" {
  description = "Provides an S3 bucket CORS configuration resource. For more information about CORS, go to Enabling Cross-Origin Resource Sharing in the Amazon S3 User Guide."
  type        = any
  default     = []
}
variable "lifecycle_configuration" {
  description = "Provides an independent configuration resource for S3 bucket lifecycle configuration."
  type        = any
  default     = []
}
variable "website_configuration" {
  description = "Provides an S3 bucket website configuration resource. For more information, see Hosting Websites on S3."
  type        = any
  default     = []
}