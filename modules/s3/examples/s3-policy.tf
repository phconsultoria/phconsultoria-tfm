module "S3WithPolicy" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/s3"
  bucket_name        = "bucket001"
  bucket_description = "Bucket of example with s3 policy"
  versioning_status  = "Enabled"
  policy             = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowCloudFrontServicePrincipalReadOnly",
            "Effect": "Allow",
            "Principal": {
                "Service": "cloudfront.amazonaws.com"
            },
            "Action": "s3:GetObject",
            "Resource": "${module.S3WithPolicy.bucket_arn}/*",
            "Condition": {
                "StringEquals": {
                    "AWS:SourceArn": "arn:aws:cloudfront::123456789123:distribution/ABCJOK1234ZABC"
                }
            }
        }
    ]
}
  POLICY
}