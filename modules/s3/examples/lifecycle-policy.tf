module "S3LifecyclePolicy" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/s3"
  bucket_name        = "bucket001"
  bucket_description = "Bucket of example with lifecycle policy configuraton"
  versioning_status  = "Enabled"

  lifecycle_configuration = [{
    id     = "Example"
    status = "Enabled"

    transition = [
      {
        days          = 30
        storage_class = "STANDARD_IA"
      },
      {
        days          = 90
        storage_class = "GLACIER"
      }
    ]
    expiration = {
      days = 120
    }
    filter = {
      prefix                   = "logs/"
      object_size_greater_than = 200000
      object_size_less_than    = 500000
      tags = {
        Name = "Sample"
        Env  = "Prod"
      }
    }
  }]
}