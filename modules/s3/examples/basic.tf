module "S3BasicBucket" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/s3"
  bucket_name        = "bucket001"
  bucket_description = "Bucket of example"
}