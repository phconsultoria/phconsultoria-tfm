module "S3StaticWebsite" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/s3"
  bucket_name        = "www.example.com.br"
  bucket_description = "Bucket of example with static website in S3"

  website_configuration = {
    index_document = "index.html"
    error_document = "error.html"

# conflicts with "index_document" and "erro_document"
#    redirect_all_requests_to = {
#      host_name = "www.phconsultoria.com.br"
#      protocol = "https"
#    }
  }
}