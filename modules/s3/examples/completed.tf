module "S3CompletedBucket" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/s3"
  bucket_name        = "bucket001"
  bucket_description = "Bucket of example"
  versioning_status  = "Enabled"
  policy             = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowCloudFrontServicePrincipalReadOnly",
            "Effect": "Allow",
            "Principal": {
                "Service": "cloudfront.amazonaws.com"
            },
            "Action": "s3:GetObject",
            "Resource": "${module.S3WithPolicy.bucket_arn}/*",
            "Condition": {
                "StringEquals": {
                    "AWS:SourceArn": "arn:aws:cloudfront::123456789123:distribution/ABCJOK1234ZABC"
                }
            }
        }
    ]
}
  POLICY

  # Cors configuration
  cors_configuration = [{
    allowed_headers = ["localhost"]
    allowed_methods = ["GET", "PUT", "DELETE", "POST", "HEAD"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag", "x-amz-id-2", "x-amz-request-id", "x-amz-server-side-encryption"]
    max_age_seconds = "3000"
  }]

  # Lifecycle configuration
  lifecycle_configuration = [{
    id     = "Example"
    status = "Enabled"

    transition = [
      {
        days          = 30
        storage_class = "STANDARD_IA"
      },
      {
        days          = 90
        storage_class = "GLACIER"
      }
    ]
    expiration = {
      days = 120
    }
    filter = {
      prefix                   = "logs/"
      object_size_greater_than = 200000
      object_size_less_than    = 500000
      tags = {
        Name = "Sample"
        Env  = "Prod"
      }
    }
  }]

  # Static website configuration
  website_configuration = {
    index_document = "index.html"
    error_document = "error.html"

# conflicts with "index_document" and "erro_document"
#    redirect_all_requests_to = {
#      host_name = "www.phconsultoria.com.br"
#      protocol = "https"
#    }
  }
}