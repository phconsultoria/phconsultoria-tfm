module "S3WithCors" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/s3"
  bucket_name        = "bucket001"
  bucket_description = "Bucket of example with cors configuration"
  versioning_status  = "Enabled"

  cors_configuration = [{
    allowed_headers = ["localhost"]
    allowed_methods = ["GET", "PUT", "DELETE", "POST", "HEAD"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag", "x-amz-id-2", "x-amz-request-id", "x-amz-server-side-encryption"]
    max_age_seconds = "3000"
  }]
}