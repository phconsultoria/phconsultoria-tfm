# Bucket S3 (Required)
resource "aws_s3_bucket" "this" {
  bucket        = var.bucket_name
  force_destroy = var.force_destroy
  tags = merge(
    var.default_tags,
    {
      Name        = var.bucket_name
      Description = var.bucket_description
    }
  )
}

# S3 block public access (Required)
resource "aws_s3_bucket_public_access_block" "this" {
  bucket                  = var.bucket_name
  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true

  depends_on = [aws_s3_bucket.this]
}

# S3 bucket encrypt (Required)
resource "aws_s3_bucket_server_side_encryption_configuration" "kms" {
  count  = var.kms_master_key_id != null ? 1 : 0
  bucket = aws_s3_bucket.this.bucket
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = var.kms_master_key_id
      sse_algorithm     = "aws:kms"
    }
  }

  depends_on = [aws_s3_bucket.this]
}
resource "aws_s3_bucket_server_side_encryption_configuration" "AES256" {
  count  = var.kms_master_key_id == null ? 1 : 0
  bucket = aws_s3_bucket.this.bucket
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }

  depends_on = [aws_s3_bucket.this]
}

# Bucket policy
resource "aws_s3_bucket_policy" "this" {
  count  = (var.policy != null) ? 1 : 0
  bucket = aws_s3_bucket.this.bucket
  policy = var.policy

  depends_on = [aws_s3_bucket.this]
}

# Lifecycle policy (Optional)
resource "aws_s3_bucket_lifecycle_configuration" "this" {
  count  = var.lifecycle_configuration != [] ? 1 : 0
  bucket = aws_s3_bucket.this.bucket
  rule {
    id     = try(var.lifecycle_configuration[count.index].id, "default-lifecycle")
    status = try(var.lifecycle_configuration[count.index].status, "Enabled")

    # Several blocks - transition
    dynamic "transition" {
      for_each = try(flatten([var.lifecycle_configuration[count.index].transition]), [])

      content {
        days          = try(transition.value.days, null)
        date          = try(transition.value.date, null)
        storage_class = transition.value.storage_class
      }
    }

    # Max 1 block - expiration
    dynamic "expiration" {
      for_each = try(flatten([var.lifecycle_configuration[count.index].expiration]), [])

      content {
        days                         = try(var.lifecycle_configuration[count.index].expiration.days, null)
        expired_object_delete_marker = try(var.lifecycle_configuration[count.index].expiration.expired_object_delete_marker, null)
      }
    }

    # Max 1 block - filter - with one key argument or a single tag
    dynamic "filter" {
      for_each = [for v in try(flatten([var.lifecycle_configuration[count.index].filter]), []) : v if max(length(keys(v)), length(try(var.lifecycle_configuration[count.index].filter.tags, var.lifecycle_configuration[count.index].filter.tag, []))) == 1]

      content {
        prefix                   = try(var.lifecycle_configuration[count.index].filter.prefix, null)
        object_size_greater_than = try(var.lifecycle_configuration[count.index].filter.object_size_greater_than, null)
        object_size_less_than    = try(var.lifecycle_configuration[count.index].filter.object_size_less_than, null)

        dynamic "tag" {
          for_each = try(var.lifecycle_configuration[count.index].filter.tags, var.lifecycle_configuration[count.index].filter.tag, [])

          content {
            key   = tag.key
            value = tag.value
          }
        }
      }
    }

    # Max 1 block - filter - with more than one key arguments or multiple tags
    dynamic "filter" {
      for_each = [for v in try(flatten([var.lifecycle_configuration[count.index].filter]), []) : v if max(length(keys(v)), length(try(var.lifecycle_configuration[count.index].filter.tags, var.lifecycle_configuration[count.index].filter.tag, []))) > 1]

      content {
        and {
          prefix                   = try(var.lifecycle_configuration[count.index].filter.prefix, null)
          object_size_greater_than = try(var.lifecycle_configuration[count.index].filter.object_size_greater_than, null)
          object_size_less_than    = try(var.lifecycle_configuration[count.index].filter.object_size_less_than, null)
          tags                     = try(var.lifecycle_configuration[count.index].filter.tags, null)
        }
      }
    }
  }

  depends_on = [aws_s3_bucket.this]
}

# Versioning (Optional)
resource "aws_s3_bucket_versioning" "this" {
  count  = var.versioning_status == "Enabled" ? 1 : 0
  bucket = var.bucket_name
  versioning_configuration {
    status = var.versioning_status
  }

  depends_on = [aws_s3_bucket.this]
}

# Website (Optional)
resource "aws_s3_bucket_website_configuration" "this" {
  count  = var.website_configuration != [] ? 1 : 0
  bucket = aws_s3_bucket.this.id

  dynamic "index_document" {
    for_each = try([var.website_configuration["index_document"]], [])

    content {
      suffix = index_document.value
    }
  }
  dynamic "error_document" {
    for_each = try([var.website_configuration["error_document"]], [])

    content {
      key = error_document.value
    }
  }
  dynamic "redirect_all_requests_to" {
    for_each = try([var.website_configuration["redirect_all_requests_to"]], [])

    content {
      host_name = redirect_all_requests_to.value.host_name
      protocol  = try(redirect_all_requests_to.value.protocol, null)
    }
  }

  routing_rules = try(var.website_configuration[count.index].routing_rules, null)

  depends_on = [aws_s3_bucket.this]
}

# Cors Configuration
resource "aws_s3_bucket_cors_configuration" "this" {
  count  = var.cors_configuration != [] ? 1 : 0
  bucket = aws_s3_bucket.this.id

  cors_rule {
    allowed_headers = try(var.cors_configuration[count.index].allowed_headers, ["*"])
    allowed_methods = try(var.cors_configuration[count.index].allowed_methods, ["GET"])
    allowed_origins = try(var.cors_configuration[count.index].allowed_origins, ["http://localhost"])
    expose_headers  = try(var.cors_configuration[count.index].expose_headers, ["ETag", "x-amz-id-2", "x-amz-request-id", "x-amz-server-side-encryption"])
    max_age_seconds = try(var.cors_configuration[count.index].max_age_seconds, 3000)
  }

  depends_on = [aws_s3_bucket.this]
}