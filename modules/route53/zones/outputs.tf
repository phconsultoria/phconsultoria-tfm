output "zone_name" {
  value = aws_route53_zone.this.name
}
output "zone_id" {
  value = aws_route53_zone.this.id
}
output "zone_zone_id" {
  value = aws_route53_zone.this.zone_id
}
output "zone_arn" {
  value = aws_route53_zone.this.arn
}