output "db_instance_address" {
  description = "The endpoint (address) of the RDS instance"
  value       = module.db_instance[*].db_instance_address
}
output "db_instance_arn" {
  description = "The ARN of the RDS instance"
  value       = module.db_instance[*].db_instance_arn
}
output "db_instance_availability_zone" {
  description = "The availability zone of the RDS instance"
  value       = module.db_instance[*].db_instance_availability_zone
}
output "db_instance_endpoint" {
  description = "The connection endpoint"
  value       = module.db_instance[*].db_instance_endpoint
}
output "db_instance_engine" {
  description = "The database engine"
  value       = module.db_instance[*].db_instance_engine
}
output "db_instance_engine_version_actual" {
  description = "The running version of the database"
  value       = module.db_instance[*].db_instance_engine_version_actual
}
output "db_instance_hosted_zone_id" {
  description = "The canonical hosted zone ID of the DB instance (to be used in a Route 53 Alias record)"
  value       = module.db_instance[*].db_instance_hosted_zone_id
}
output "db_instance_id" {
  description = "The RDS instance ID"
  value       = module.db_instance[*].db_instance_id
}
output "db_instance_resource_id" {
  description = "The RDS Resource ID of this[0] instance"
  value       = module.db_instance[*].db_instance_resource_id
}
output "db_instance_status" {
  description = "The RDS instance status"
  value       = module.db_instance[*].db_instance_status
}
output "db_instance_name" {
  description = "The database name"
  value       = module.db_instance[*].db_instance_name
}
output "db_instance_username" {
  description = "The master username for the database"
  value       = module.db_instance[*].db_instance_username
  sensitive   = true
}
output "db_instance_port" {
  description = "The database port"
  value       = module.db_instance[*].db_instance_port
}
output "secretmanager_id" {
  description = "The ID of Secret Manager"
  value       = module.db_instance[*].secretmanager_id
}
output "secretmanager_arn" {
  description = "The ARN of Secret Manager"
  value       = module.db_instance[*].secretmanager_arn
}