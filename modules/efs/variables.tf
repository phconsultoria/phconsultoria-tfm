# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED VARIABLES
# These variables must be set when using this module.
# ---------------------------------------------------------------------------------------------------------------------
variable "create_efs" {
  description = "Controls if the EFS volume should be created"
  type        = bool
  default     = true
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL VARIABLES
# These variables have defaults, but may be overridden.
# ---------------------------------------------------------------------------------------------------------------------
variable "aws_efs_file_system" {
  description = "Provides an Elastic File System (EFS) File System resource."
  type        = any
  default     = []
}
variable "aws_efs_backup_policy" {
  description = "Provides an Elastic File System (EFS) Backup Policy resource. Backup policies turn automatic backups on or off for an existing file system."
  type        = any
  default     = []
}
variable "aws_efs_file_system_policy" {
  description = "Provides an Elastic File System (EFS) File System Policy resource."
  type        = any
  default     = []
}
variable "aws_efs_mount_target" {
  description = "Provides an Elastic File System (EFS) mount target."
  type        = any
  default     = []
}
variable "aws_efs_replication_configuration" {
  description = "Creates a replica of an existing EFS file system in the same or another region."
  type        = any
  default     = []
}
variable "aws_efs_access_point" {
  description = "Provides an Elastic File System (EFS) access point."
  type        = any
  default     = []
}