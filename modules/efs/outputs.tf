# ---------------------------------------------------------------------------------------------------------------------
# OUTPUTS
# ---------------------------------------------------------------------------------------------------------------------

output "efs_id" {
  value = aws_efs_file_system.this[*].id
}
output "efs_arn" {
  value = aws_efs_file_system.this[*].arn
}
output "efs_create_token" {
  value = aws_efs_file_system.this[*].creation_token
}
output "efs_dns_name" {
  value = aws_efs_file_system.this[*].dns_name
}
output "efs_mount_target_subnet_id" {
  value = aws_efs_mount_target.this[*].subnet_id
}
output "efs_mount_target_subnet_ip_address" {
  value = aws_efs_mount_target.this[*].ip_address
}