# ---------------------------------------------------------------------------------------------------------------------
# EFS (Elastic File System)
# ---------------------------------------------------------------------------------------------------------------------

# Filesystem
resource "aws_efs_file_system" "this" {
  count                           = var.create_efs ? length(var.aws_efs_file_system) : 0
  availability_zone_name          = try(var.aws_efs_file_system[count.index].availability_zone_name, null)
  creation_token                  = try(var.aws_efs_file_system[count.index].availability_zone_name, null)
  encrypted                       = try(var.aws_efs_file_system[count.index].encrypted, false)
  kms_key_id                      = try(var.aws_efs_file_system[count.index].kms_key_id, null)
  performance_mode                = try(var.aws_efs_file_system[count.index].performance_mode, "generalPurpose")
  throughput_mode                 = try(var.aws_efs_file_system[count.index].provisioned_throughput_in_mibps, null)
  provisioned_throughput_in_mibps = try(var.aws_efs_file_system[count.index].provisioned_throughput_in_mibps, null)
  tags = try(var.aws_efs_file_system[count.index].tags, {
    CreatedBy       = "Terraform"
    TerraformModule = "https://gitlab.com/phconsultoria/phconsultoria-tfm/-/tree/main/modules/efs"
  })

  protection {
    replication_overwrite = try(var.aws_efs_file_system[count.index].protection.replication_overwrite, "DISABLED")
  }

  dynamic "lifecycle_policy" {
    for_each = try([var.aws_efs_file_system[count.index].lifecycle_policy], [])
    content {
      transition_to_ia                    = try(var.aws_efs_file_system[count.index].lifecycle_policy.transition_to_ia, null)
      transition_to_primary_storage_class = try(var.aws_efs_file_system[count.index].lifecycle_policy.transition_to_primary_storage_class, null)
      transition_to_archive               = try(var.aws_efs_file_system[count.index].lifecycle_policy.transition_to_archive, null)
    }
  }

  connection {
    timeouts {
      create = "10m"
      delete = "10h"
    }
  }
}

# Filesystem Mount Target
resource "aws_efs_mount_target" "this" {
  count           = var.create_efs ? length(var.aws_efs_mount_target) : 0
  file_system_id  = aws_efs_file_system.this[0].id
  subnet_id       = try(var.aws_efs_mount_target[count.index].subnet_id, "")
  ip_address      = try(var.aws_efs_mount_target[count.index].ip_address, null)
  security_groups = try(var.aws_efs_mount_target[count.index].security_groups, null)

  connection {
    timeouts {
      create = "10m"
      delete = "10h"
    }
  }
}

# Filesystem Policy
data "aws_iam_policy_document" "this" {
  statement {
    sid    = "RW"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "elasticfilesystem:ClientMount",
      "elasticfilesystem:ClientWrite",
      "elasticfilesystem:ClientRootAccess"
    ]

    resources = [aws_efs_file_system.this[0].arn]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["true"]
    }
  }
}
resource "aws_efs_file_system_policy" "this" {
  count          = var.create_efs ? length(var.aws_efs_file_system_policy) : 0
  file_system_id = aws_efs_file_system.this[0].id
  policy         = try(var.aws_efs_file_system_policy[count.index].policy, data.aws_iam_policy_document.this.json)

  connection {
    timeouts {
      create = "10m"
      delete = "10h"
    }
  }
}

# Filesystem Backup Policy
resource "aws_efs_backup_policy" "this" {
  count          = var.create_efs ? length(var.aws_efs_backup_policy) : 0
  file_system_id = aws_efs_file_system.this[0].id

  backup_policy {
    status = try(var.aws_efs_backup_policy[count.index].backup_policy.status, "ENABLED")
  }

  connection {
    timeouts {
      create = "10m"
      delete = "10h"
    }
  }
}

# Filesystem replication
resource "aws_efs_replication_configuration" "this" {
  count                 = var.create_efs ? length(var.aws_efs_replication_configuration) : 0
  source_file_system_id = aws_efs_file_system.this[0].id

  destination {
    region                 = try(var.aws_efs_replication_configuration[count.index].destination.region, "us-east-2")
    kms_key_id             = try(var.aws_efs_replication_configuration[count.index].destination.kms_key_id, null)
    availability_zone_name = try(var.aws_efs_replication_configuration[count.index].destination.availability_zone_name, null)
    file_system_id         = try(var.aws_efs_replication_configuration[count.index].destination.file_system_id, null)
  }

  connection {
    timeouts {
      create = "10m"
      delete = "10h"
    }
  }
}

# Filesystem Access Point
resource "aws_efs_access_point" "this" {
  count          = var.create_efs ? length(var.aws_efs_access_point) : 0
  file_system_id = aws_efs_file_system.this[0].id
  tags = try(var.aws_efs_access_point[count.index].tags, {
    CreatedBy       = "Terraform"
    TerraformModule = "https://gitlab.com/phconsultoria/phconsultoria-tfm/-/tree/main/modules/efs"
  })
  posix_user {
    gid            = try(var.aws_efs_access_point[count.index].posix_user.gid, 0)
    uid            = try(var.aws_efs_access_point[count.index].posix_user.uid, 0)
    secondary_gids = try(var.aws_efs_access_point[count.index].posix_user.secondary_gids, null)
  }
  root_directory {
    creation_info {
      owner_gid   = try(var.aws_efs_access_point[count.index].root_directory.creation_info.owner_gid, 0)
      owner_uid   = try(var.aws_efs_access_point[count.index].root_directory.creation_info.owner_uid, 0)
      permissions = try(var.aws_efs_access_point[count.index].root_directory.creation_info.permissions, 0775)
    }
    path = try(var.aws_efs_access_point[count.index].root_directory.path, null)
  }

  connection {
    timeouts {
      create = "10m"
      delete = "10h"
    }
  }
}