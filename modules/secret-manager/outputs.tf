# ---------------------------------------------------------------------------------------------------------------------
# OUTPUTS
# ---------------------------------------------------------------------------------------------------------------------

output "id" {
  value = aws_secretsmanager_secret.this.id
}
output "arn" {
  value = aws_secretsmanager_secret.this.arn
}
output "secrets_strings" {
  value = aws_secretsmanager_secret_version.this.secret_string
}