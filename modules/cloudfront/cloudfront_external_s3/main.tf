# ---------------------------------------------------------------------------------------------------------------------
# CLOUDFRONT
# ---------------------------------------------------------------------------------------------------------------------

# Cloudfront Distribution
resource "aws_cloudfront_distribution" "this" {
  count               = var.create_cf ? 1 : 0
  enabled             = try(var.global_settings[count.index].enabled, true)
  is_ipv6_enabled     = try(var.global_settings[count.index].is_ipv6_enabled, true)
  comment             = try(var.global_settings[count.index].comment, "Cloudfront Distribution")
  default_root_object = try(var.global_settings[count.index].default_root_object, "index.html")
  http_version        = try(var.global_settings[count.index].http_version, "http2")
  aliases             = try(var.global_settings[count.index].aliases, "http2")
  tags = try(var.global_settings[count.index].tags, {
    CreatedBy       = "Terraform"
    TerraformModule = "https://gitlab.com/phconsultoria/phconsultoria-tfm/-/tree/main/modules/cloudfront/cloudfront_only"
  })

  default_cache_behavior {
    response_headers_policy_id = try(var.default_cache_behavior[count.index].response_headers_policy_id, null)
    allowed_methods            = try(var.default_cache_behavior[count.index].allowed_methods, ["HEAD", "GET"])
    cached_methods             = try(var.default_cache_behavior[count.index].cached_methods, ["HEAD", "GET"])
    target_origin_id           = var.origin[0].domain_name
    viewer_protocol_policy     = try(var.default_cache_behavior[count.index].viewer_protocol_policy, "redirect-to-https")
    compress                   = try(var.default_cache_behavior[count.index].compress, true)
    cache_policy_id            = aws_cloudfront_cache_policy.this[0].id
  }
  origin {
    domain_name              = try(var.origin[count.index].domain_name, "none")
    origin_id                = try(var.origin[count.index].domain_name, "none")
    origin_path              = try(var.origin[count.index].origin_path, "")
    connection_attempts      = try(var.origin[count.index].connection_attempts, "3")
    connection_timeout       = try(var.origin[count.index].connection_timeout, "10")
    origin_access_control_id = aws_cloudfront_origin_access_control.this[0].id
  }
  restrictions {
    geo_restriction {
      restriction_type = try(var.geo_restriction[count.index].restriction_type, "none")
      locations        = try(var.geo_restriction[count.index].locations, null)
    }
  }
  viewer_certificate {
    cloudfront_default_certificate = try(var.view_certificate[count.index].cloudfront_default_certificate, false)
    minimum_protocol_version       = try(var.view_certificate[count.index].minimum_protocol_version, "TLSv1.2_2021")
    ssl_support_method             = try(var.view_certificate[count.index].ssl_support_method, "sni-only")
    acm_certificate_arn            = try(var.view_certificate[count.index].acm_certificate_arn, null)
  }
}

# Cache policy
resource "aws_cloudfront_cache_policy" "this" {
  count       = var.create_cf ? 1 : 0
  name        = try(var.cache_policy[count.index].name, null)
  comment     = try(var.cache_policy[count.index].comment, "Custom policy cache for Cloudfront")
  default_ttl = try(var.cache_policy[count.index].default_ttl, "3600")
  max_ttl     = try(var.cache_policy[count.index].max_ttl, "3600")
  min_ttl     = try(var.cache_policy[count.index].min_ttl, "1")
  parameters_in_cache_key_and_forwarded_to_origin {
    enable_accept_encoding_gzip   = try(var.cache_policy[count.index].parameters_in_cache_key_and_forwarded_to_origin.enable_accept_encoding_gzip, "true")
    enable_accept_encoding_brotli = try(var.cache_policy[count.index].parameters_in_cache_key_and_forwarded_to_origin.enable_accept_encoding_brotli, "true")
    query_strings_config {
      query_string_behavior = try(var.cache_policy[count.index].parameters_in_cache_key_and_forwarded_to_origin.query_strings_config.query_string_behavior, "none")
    }
    cookies_config {
      cookie_behavior = try(var.cache_policy[count.index].parameters_in_cache_key_and_forwarded_to_origin.cookies_config.cookie_behavior, "none")
    }
    headers_config {
      header_behavior = try(var.cache_policy[count.index].parameters_in_cache_key_and_forwarded_to_origin.headers_config.header_behavior, "none")
    }
  }
}

# Origin Access Control
resource "aws_cloudfront_origin_access_control" "this" {
  count                             = var.create_cf ? 1 : 0
  name                              = try(var.origin_access_control[count.index].name, null)
  description                       = try(var.origin_access_control[count.index].description, null)
  origin_access_control_origin_type = try(var.origin_access_control[count.index].origin_access_control_origin_type, "s3")
  signing_behavior                  = try(var.origin_access_control[count.index].signing_behavior, "always")
  signing_protocol                  = try(var.origin_access_control[count.index].signing_protocol, "sigv4")
}