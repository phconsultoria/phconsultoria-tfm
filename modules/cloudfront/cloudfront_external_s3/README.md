[<img src="https://gitlab.com/uploads/-/system/project/avatar/40750820/logoAZUL_fundoTransparente-pH22.png" width="200"/>](https://www.phconsultoria.com.br)


# Cloudfront (static website with Amazon S3)

A [Terraform](https://www.terraform.io) module to  create a static website using [Cloudfront Distribution](https://aws.amazon.com/pt/cloudfront/) and
[Amazon S3](https://aws.amazon.com/pt/s3/).
<br>
<br>
Attention: In this case, S3 Bucket  he must be created in any AWS account. <br>
<br>



[![Terraform Version](https://img.shields.io/badge/terraform-1.5.7%20+-623CE4.svg?logo=terraform)](https://github.com/hashicorp/terraform/releases)
[![AWS Provider Version](https://img.shields.io/badge/AWS-5.18.1+-F8991D.svg?logo=terraform)](https://github.com/terraform-providers/terraform-provider-aws/releases)
<br>


## Getting Started
This is a simple sample, with minimum necessary options. Please read and change as needed.
```bash
module "Sample" {
  source   = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/cloudfront/cloudfront_external_s3"
  global_settings = [{
    aliases = ["sample.domain.com"]
  }]
  view_certificate = [{
    acm_certificate_arn = "arn:aws:acm:us-east-1:111111111111:certificate/certificate-id"
    }]
  origin = [{
    domain_name = "sample.s3.us-east-1.amazonaws.com"
  }]
  cache_policy = [{
    name = "sample-policy"
  }]
  origin_access_control = [{
    name        = "sample"
    description = "OAC Sample"
  }]
}
```


## References
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution 
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_origin_access_identity
<br>

## License

[![License](https://img.shields.io/badge/License-Apache2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)


This module is licensed under the Apache License Version 2.0, January 2004.
Please see [LICENSE](LICENSE) for full details.

Copyright &copy; 2023  [pH Consultoria](https://www.phconsultoria.com.br)
