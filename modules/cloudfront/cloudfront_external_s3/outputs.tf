# ---------------------------------------------------------------------------------------------------------------------
# CLOUDFRONT
# ---------------------------------------------------------------------------------------------------------------------

output "cloudfront_id" {
  value = aws_cloudfront_distribution.this[0].id
}
output "cloudfront_arn" {
  value = aws_cloudfront_distribution.this[0].arn
}
output "cloudfront_domain_name" {
  value = aws_cloudfront_distribution.this[0].domain_name
}