# ---------------------------------------------------------------------------------------------------------------------
# CONTROL VARIABLES
# ---------------------------------------------------------------------------------------------------------------------

variable "create_cf" {
  description = "Controls if the Cloudfront should be created"
  type        = bool
  default     = true
}

# ---------------------------------------------------------------------------------------------------------------------
# CLOUDFRONT VARIABLES
# ---------------------------------------------------------------------------------------------------------------------

variable "origin" {
  description = "Origin from Cloudfront"
  type        = any
  default     = []
}
variable "global_settings" {
  description = "Session for global settings from Cloudfront"
  type        = any
  default     = []
}
variable "view_certificate" {
  description = "Session for TLS certificate settings"
  type        = any
  default     = []
}
variable "origin_access_control" {
  description = "Manages an AWS CloudFront Origin Access Control, which is used by CloudFront Distributions with an Amazon S3 bucket as the origin."
  type        = any
  default     = []
}
variable "cache_policy" {
  description = "Use the aws_cloudfront_cache_policy resource to create a cache policy for CloudFront."
  type        = any
  default     = []
}
variable "geo_restriction" {
  description = "Geo Location restriction session"
  type        = any
  default     = []
}
variable "default_cache_behavior" {
  description = "Session for default cache behavior"
  type        = any
  default     = []
}