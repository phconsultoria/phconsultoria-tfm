[<img src="https://gitlab.com/uploads/-/system/project/avatar/40750820/logoAZUL_fundoTransparente-pH22.png" width="200"/>](https://www.phconsultoria.com.br)


# EC2

A [Terraform](https://www.terraform.io) modules to create and manage [Amazon EC2 Instances](https://aws.amazon.com/ec2/), [Data Lifecycle Manager](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/snapshot-lifecycle.html),
and [EBS Volumes](https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/ebs-volumes.html) in [Amazon Web Services (AWS)](https://aws.amazon.com/). <br>
<br>


[![Terraform Version](https://img.shields.io/badge/terraform-1.3.4%20+-623CE4.svg?logo=terraform)](https://github.com/hashicorp/terraform/releases)
[![AWS Provider Version](https://img.shields.io/badge/AWS-4.38.0+-F8991D.svg?logo=terraform)](https://github.com/terraform-providers/terraform-provider-aws/releases)
[![Owner](https://img.shields.io/badge/Developed%20by-https://www.phconsultoria.com.br-blue)](https://www.phconsultoria.com.br)


## Getting Started
This is a simple sample, with minimum necessary options. Please read and change as needed.
* Instances
```bash
module "SampleEC2" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/ec2/instance"
  ec2_name           = ""
  ami                = ""
  ec2_type           = ""
  security_group_ids = []
  subnet_id          = ""
  volume_size        = ""
}
```
* DLM
```bash
module "Sample" {
  source             = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/ec2/dlm"  
  name               = ""
  target_tags = {}
}
``` 
* EBS Volumes
```bash
module "Sample" {
  source      = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/ec2/ebs"  
  aws_ebs_volume = [{
    availability_zone = "us-east-1a"
    size              = 20
    type              = "gp3"
    encrypted         = true
    kms_key_id        = ""
    tags = merge(
      var.default_tags,
      {
        Name     = "sample"
      }
    )
  }]
  aws_volume_attachment = [{
    device_name = "/dev/sdh"
    volume_id   = module.Sample.ebs_volume_id[0]
  }]
}
```

* ELB
```bash
module "Sample_ALB" {
  source      = "git::https://gitlab.com/phconsultoria/phconsultoria-tfm.git//modules/ec2/elb"  
  lb = [{
    name            = "sample_elb"
    security_groups = ["security_group_id"]
    subnets         = ["subnet_id"]
    tags = merge(
      var.default_tags,
      {
        Name = "sample-elb"
      }
    )
  }]
  target_groups = [{
    name             = "sample-tg"
    port             = "80"
    protocol         = "http"
    protocol_version = "http2"
    vpc_id           = "vpc_id"
    tags = merge(
      var.default_tags,
      {
        Name = "sample-tg"
      }
    )
    health_check = {
      port = "80"
    }
  }]
  targets = [{
    target_id = "i-12303c51926be9999"
    port = "80"
  }]
  listener = [
    {
      port     = "80"
      protocol = "HTTP"
      default_action = {
        type = "redirect"
        redirect = {
          port        = "443"
          protocol    = "HTTPS"
          status_code = "HTTP_301"
        }
      }
    },
    {
      port            = "443"
      protocol        = "HTTPS"
      certificate_arn = aws_acm_certificate.xyz.arn
      ssl_policy      = "ELBSecurityPolicy-TLS13-1-2-2021-06"
      default_action = {
        type = "forward"
      }
    }
  ]
}
```

## References
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dlm_lifecycle_policy
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ebs_volume
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/volume_attachment

## License

[![License](https://img.shields.io/badge/License-Apache2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)

This module is licensed under the Apache License Version 2.0, January 2004.
Please see [LICENSE](LICENSE) for full details.

Copyright &copy; 2022  [pH Consultoria](https://www.phconsultoria.com.br)
