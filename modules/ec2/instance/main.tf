# ---------------------------------------------------------------------------------------------------------------------
# INSTANCE
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_instance" "this" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  associate_public_ip_address = var.public_ip
  disable_api_termination     = var.api_termination
  disable_api_stop            = var.api_stop
  source_dest_check           = var.source_dest
  vpc_security_group_ids      = var.security_group_ids
  subnet_id                   = var.subnet_id
  user_data                   = var.user_data
  iam_instance_profile        = var.iam_profile
  availability_zone           = var.availability_zone

  metadata_options {
    http_endpoint               = try(var.metadata_options.http_endpoint, "enabled")
    http_tokens                 = try(var.metadata_options.http_tokens, "required")
    http_put_response_hop_limit = try(var.metadata_options.http_put_response_hop_limit, 1)
    instance_metadata_tags      = try(var.metadata_options.instance_metadata_tags, "enabled")
  }

  tags = merge(
    var.default_tags,
    {
      Name        = var.ec2_name
      Description = var.ec2_description
    }
  )

  # Important: Modifying any of the root_block_device settings other than volume_size or
  # tags requires resource replacement.
  # Doc: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance#ebs-ephemeral-and-root-block-devices

  root_block_device {
    delete_on_termination = var.delete_on_termination
    encrypted             = var.encrypted
    volume_size           = var.volume_size
    volume_type           = var.volume_type
    kms_key_id            = var.encrypted ? var.kms_key_id : null
    tags = merge(
      var.ebs_tags,
      {
        Name = "${var.ec2_name}-root"
      }
    )
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# EBS VOLUMES
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_ebs_volume" "this" {
  count                = var.create_ec2 ? length(var.aws_ebs_volume) : 0
  availability_zone    = try(var.aws_ebs_volume[count.index].availability_zone, null)
  size                 = try(var.aws_ebs_volume[count.index].size, "10")
  type                 = try(var.aws_ebs_volume[count.index].type, "gp3")
  encrypted            = try(var.aws_ebs_volume[count.index].encrypted, false)
  kms_key_id           = try(var.aws_ebs_volume[count.index].kms_key_id, null)
  final_snapshot       = try(var.aws_ebs_volume[count.index].final_snapshot, false)
  iops                 = try(var.aws_ebs_volume[count.index].iops, null)
  multi_attach_enabled = try(var.aws_ebs_volume[count.index].multi_attach_enabled, false)
  snapshot_id          = try(var.aws_ebs_volume[count.index].snapshot_id, null)
  outpost_arn          = try(var.aws_ebs_volume[count.index].outpost_arn, null)
  throughput           = try(var.aws_ebs_volume[count.index].throughput, null)
  tags = try(var.aws_ebs_volume[count.index].tags, {
    CreatedBy       = "Terraform"
    TerraformModule = "https://gitlab.com/phconsultoria/phconsultoria-tfm/-/tree/main/modules/ec2/instance"
  })

  depends_on = [aws_instance.this]
}
resource "aws_volume_attachment" "this" {
  count                          = var.create_ec2 ? length(var.aws_ebs_volume) : 0
  device_name                    = try(var.aws_ebs_volume[count.index].device_name, "/dev/sdh")
  volume_id                      = try(var.aws_ebs_volume[count.index].volume_id, aws_ebs_volume.this[0].id)
  instance_id                    = aws_instance.this.id
  force_detach                   = try(var.aws_ebs_volume[count.index].force_detach, false)
  skip_destroy                   = try(var.aws_ebs_volume[count.index].skip_destroy, false)
  stop_instance_before_detaching = try(var.aws_ebs_volume[count.index].stop_instance_before_detaching, false)

  depends_on = [aws_instance.this, aws_ebs_volume.this]
}

# ---------------------------------------------------------------------------------------------------------------------
# ELASTIC IP
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_eip" "this" {
  count                     = var.create_ec2 ? length(var.aws_eip) : 0
  domain                    = try(var.aws_eip[count.index].domain, "vpc")
  network_interface         = try(var.aws_eip[count.index].network_interface, null)
  public_ipv4_pool          = try(var.aws_eip[count.index].public_ipv4_pool, "amazon")
  associate_with_private_ip = try(var.aws_eip[count.index].associate_with_private_ip, null)
  tags = try(var.aws_eip[count.index].tags, {
    CreatedBy       = "Terraform"
    TerraformModule = "https://gitlab.com/phconsultoria/phconsultoria-tfm/-/tree/main/modules/ec2/instance"
  })
}
resource "aws_eip_association" "this" {
  count                = var.create_ec2 ? length(var.aws_eip) : 0
  allow_reassociation  = try(var.aws_eip[count.index].allow_reassociation, true)
  network_interface_id = try(var.aws_eip[count.index].network_interface_id, null)
  private_ip_address   = try(var.aws_eip[count.index].private_ip_address, null)
  public_ip            = try(var.aws_eip[count.index].public_ip, null)
  instance_id          = aws_instance.this.id
  allocation_id        = aws_eip.this[count.index].id
}