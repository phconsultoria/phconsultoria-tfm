# ---------------------------------------------------------------------------------------------------------------------
# EBS VOLUMES
# ---------------------------------------------------------------------------------------------------------------------

# EBS volume
resource "aws_ebs_volume" "this" {
  count                = var.create_volume ? length(var.aws_ebs_volume) : 0
  availability_zone    = try(var.aws_ebs_volume[count.index].availability_zone, null)
  size                 = try(var.aws_ebs_volume[count.index].size, "10")
  type                 = try(var.aws_ebs_volume[count.index].type, "gp3")
  encrypted            = try(var.aws_ebs_volume[count.index].encrypted, false)
  kms_key_id           = try(var.aws_ebs_volume[count.index].kms_key_id, null)
  final_snapshot       = try(var.aws_ebs_volume[count.index].final_snapshot, false)
  iops                 = try(var.aws_ebs_volume[count.index].iops, null)
  multi_attach_enabled = try(var.aws_ebs_volume[count.index].multi_attach_enabled, false)
  snapshot_id          = try(var.aws_ebs_volume[count.index].snapshot_id, null)
  outpost_arn          = try(var.aws_ebs_volume[count.index].outpost_arn, null)
  throughput           = try(var.aws_ebs_volume[count.index].throughput, null)
  tags = try(var.aws_ebs_volume[count.index].tags, {
    CreatedBy       = "Terraform"
    TerraformModule = "https://gitlab.com/phconsultoria/phconsultoria-tfm/-/tree/main/modules/ec2/instance"
  })
}
resource "aws_volume_attachment" "this" {
  count                          = var.create_volume ? length(var.aws_volume_attachment) : 0
  device_name                    = try(var.aws_volume_attachment[count.index].device_name, "/dev/sdh")
  volume_id                      = try(var.aws_volume_attachment[count.index].volume_id, aws_ebs_volume.this[0].id)
  instance_id                    = try(var.aws_volume_attachment[count.index].instance_id, null)
  force_detach                   = try(var.aws_volume_attachment[count.index].force_detach, false)
  skip_destroy                   = try(var.aws_volume_attachment[count.index].skip_destroy, false)
  stop_instance_before_detaching = try(var.aws_volume_attachment[count.index].stop_instance_before_detaching, false)
}