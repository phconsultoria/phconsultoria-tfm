# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED VARIABLES
# These variables must be set when using this module.
# ---------------------------------------------------------------------------------------------------------------------
variable "create_volume" {
  description = "Controls if the EC2 volume should be created"
  type        = bool
  default     = true
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL VARIABLES
# These variables have defaults, but may be overridden.
# ---------------------------------------------------------------------------------------------------------------------
variable "aws_ebs_volume" {
  description = "Manages a single EBS volume."
  type        = any
  default     = []
}
variable "aws_volume_attachment" {
  description = "Provides an AWS EBS Volume Attachment as a top level resource, to attach and detach volumes from AWS Instances."
  type        = any
  default     = []
}