# ---------------------------------------------------------------------------------------------------------------------
# OUTPUTS
# ---------------------------------------------------------------------------------------------------------------------

output "volume_id" {
  value = aws_ebs_volume.this[*].id
}
output "volume_arn" {
  value = aws_ebs_volume.this[*].arn
}
output "volume_type" {
  value = aws_ebs_volume.this[*].type
}
output "volume_size" {
  value = aws_ebs_volume.this[*].size
}